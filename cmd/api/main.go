package main

import (
	"boilerplate/config"
	graphql "boilerplate/internal/server/graphql"
	grpc "boilerplate/internal/server/grpc"
	"boilerplate/pkg/infra/db"
	"boilerplate/pkg/infra/logger"
	"sync"
)

func main() {

	//* ====================== Config ======================

	conf := config.InitConfig("dev")

	//* ====================== Logger ======================

	//* Loggrus
	appLogger := logger.NewLogrusLogger(&conf.Logger.Logrus)

	//* ====================== Connection DB ======================
	var dbList db.DatabaseList
	dbPostgre := db.NewGORMConnection(&conf.Connection.DatabaseType, appLogger, "PostgreDB")
	dbStoone := db.NewGORMConnection(&conf.Connection.DatabaseType, appLogger, "StooneDB")

	dbList = db.DatabaseList{
		PostgreDB: dbPostgre,
		StooneDB:  dbStoone,
	}
	//* ====================== Running Server ======================
	wg := new(sync.WaitGroup)
	wg.Add(2)

	go func() {
		//grpc server
		grpc.Run(conf, &dbList, appLogger)
		wg.Done()
	}()

	go func() {
		//graphql server
		graphql.Run(conf, &dbList, appLogger)
		wg.Done()
	}()

	wg.Wait()
}
