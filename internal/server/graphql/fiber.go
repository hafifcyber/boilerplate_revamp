package grpc

import (
	"boilerplate/config"
	generated "boilerplate/internal/graph/generated"
	graph "boilerplate/internal/graph/resolver"
	bookPb "boilerplate/internal/grpc/pb/book"
	userPb "boilerplate/internal/grpc/pb/user"
	"boilerplate/pkg/infra/db"
	"fmt"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"log"
	"net/http"
)

func Run(conf *config.Config, dbList *db.DatabaseList, appLoger *logrus.Logger) {
	grpcAddress := fmt.Sprintf("%s:%s", "0.0.0.0", conf.App.PortGRPC)
	port := "0.0.0.0:" + conf.App.PortGraphql

	conn, err := grpc.Dial(grpcAddress, grpc.WithInsecure())
	if err != nil {
		panic(err.Error())
	}

	bookgrpcclient := bookPb.NewBooksClient(conn)
	usergrpcclient := userPb.NewUsersClient(conn)

	resolver := &graph.Resolver{
		GRPCBookClient: bookgrpcclient,
		GRPCUserClient: usergrpcclient,
	}

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: resolver}))

	http.Handle("/", playground.Handler("GraphQL playground", "/query"))
	http.Handle("/query", srv)

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(port, nil))
}
