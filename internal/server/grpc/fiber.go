package grpc

import (
	"boilerplate/config"
	bookPb "boilerplate/internal/grpc/pb/book"
	userPb "boilerplate/internal/grpc/pb/user"
	"boilerplate/internal/wrapper/handler"
	"boilerplate/internal/wrapper/repository"
	"boilerplate/internal/wrapper/usecase"
	"boilerplate/pkg/infra/db"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"

	"github.com/sirupsen/logrus"
)

func Run(conf *config.Config, dbList *db.DatabaseList, appLoger *logrus.Logger) {
	port := ":" + conf.App.PortGRPC

	if dbList == nil {
		log.Println("is nil")
	}

	repo := repository.NewRepository(conf, dbList, appLoger)
	usecase := usecase.NewUsecase(repo, conf, dbList, appLoger)
	handler := handler.NewHandler(usecase, conf, appLoger)

	//* Register server
	srv := grpc.NewServer()
	bookPb.RegisterBooksServer(srv, handler.Core.Book)
	userPb.RegisterUsersServer(srv, handler.Core.User)
	//* Reflection
	reflection.Register(srv)
	appLoger.Info("Starting RPC server at", port)

	l, err := net.Listen("tcp", port)
	if err != nil {
		appLoger.Fatalf("could not listen to %s: %v", port, err)
	}
	appLoger.Fatal(srv.Serve(l))
}
