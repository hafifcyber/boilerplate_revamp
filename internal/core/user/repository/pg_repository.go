package repository

import (
	"boilerplate/internal/core/user/models"
	"boilerplate/pkg/infra/db"
)

type Repository interface {
	GetAll() (*[]models.User, error)
}

type UserRepo struct {
	DBList *db.DatabaseList
}

func NewUserRepo(dbList *db.DatabaseList) UserRepo {
	return UserRepo{
		DBList: dbList,
	}
}

func (r UserRepo) GetAll() (*[]models.User, error) {
	var response []models.User
	err := r.DBList.PostgreDB.Raw(qGetAllUsers).Scan(&response).Error
	if err != nil {
		return nil, err
	}

	return &response, err
}
