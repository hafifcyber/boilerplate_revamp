package delivery

import (
	"boilerplate/config"
	userPb "boilerplate/internal/grpc/pb/user"
	"boilerplate/internal/wrapper/usecase"
	"context"
	"errors"
	"github.com/sirupsen/logrus"
	"google.golang.org/protobuf/types/known/emptypb"
)

type UserHandler struct {
	Usecase usecase.Usecase
	Conf    *config.Config
	Log     *logrus.Logger
	userPb.UnimplementedUsersServer
}

func NewUserHandler(uc usecase.Usecase, conf *config.Config, logger *logrus.Logger) UserHandler {
	return UserHandler{
		Usecase:                  uc,
		Conf:                     conf,
		Log:                      logger,
		UnimplementedUsersServer: userPb.UnimplementedUsersServer{},
	}
}

func (h UserHandler) GetAll(ctx context.Context, req *emptypb.Empty) (*userPb.GetUsersResponse, error) {
	//* Usecase
	data, errData := h.Usecase.Core.User.GetAll(ctx, req)
	if errData != nil {
		//* Return Error Response
		return nil, errors.New("error fetch data")
	}

	//* Success Response
	respData := &userPb.GetUsersResponse{
		Users: data,
	}

	return respData, nil
}
