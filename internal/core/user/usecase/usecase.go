package usecase

import (
	"boilerplate/config"
	userPb "boilerplate/internal/grpc/pb/user"
	repo "boilerplate/internal/wrapper/repository"
	"boilerplate/pkg/infra/db"
	"context"
	"errors"
	"github.com/sirupsen/logrus"
	"google.golang.org/protobuf/types/known/emptypb"
)

type Usecase interface {
	GetAll(ctx context.Context, req *emptypb.Empty) ([]*userPb.User, error)
}

type UserUsecase struct {
	Repo   repo.Repository
	Conf   *config.Config
	DBList *db.DatabaseList
	Log    *logrus.Logger
}

func NewUserUsecase(repository repo.Repository, conf *config.Config, dbList *db.DatabaseList, logger *logrus.Logger) UserUsecase {
	return UserUsecase{
		Repo:   repository,
		Conf:   conf,
		DBList: dbList,
		Log:    logger,
	}
}

func (u UserUsecase) GetAll(ctx context.Context, req *emptypb.Empty) ([]*userPb.User, error) {
	//* Get user data
	userData, err := u.Repo.Core.User.GetAll()
	if err != nil {
		return nil, errors.New("error fetch data")
	}
	//convert data to array
	var datas []*userPb.User
	for _, val := range *userData {
		data := &userPb.User{
			ID:   int32(val.ID),
			Name: val.Name,
		}
		datas = append(datas, data)
	}

	return datas, nil
}
