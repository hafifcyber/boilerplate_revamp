package usecase

import (
	"boilerplate/config"
	bookPb "boilerplate/internal/grpc/pb/book"
	repo "boilerplate/internal/wrapper/repository"
	"boilerplate/pkg/infra/db"
	"context"
	errors "errors"
	"github.com/sirupsen/logrus"
	"google.golang.org/protobuf/types/known/emptypb"
)

type Usecase interface {
	GetAll(ctx context.Context, req *emptypb.Empty) ([]*bookPb.Book, error)
	Get(ctx context.Context, req *bookPb.GetBookRequest) (*bookPb.Book, error)
}

type BookUsecase struct {
	Repo   repo.Repository
	Conf   *config.Config
	DBList *db.DatabaseList
	Log    *logrus.Logger
}

func NewBookUsecase(repository repo.Repository, conf *config.Config, dbList *db.DatabaseList, logger *logrus.Logger) BookUsecase {
	return BookUsecase{
		Repo:   repository,
		Conf:   conf,
		DBList: dbList,
		Log:    logger,
	}
}

func (b BookUsecase) GetAll(ctx context.Context, req *emptypb.Empty) ([]*bookPb.Book, error) {
	//* Get book data
	bookData, err := b.Repo.Core.Book.GetAll()
	if err != nil {
		return nil, errors.New("error fetch data")
	}
	//convert data to array
	var datas []*bookPb.Book
	for _, val := range *bookData {
		data := &bookPb.Book{
			ID:    val.BookID,
			Title: val.BookName,
		}
		datas = append(datas, data)
	}

	return datas, nil
}

func (b BookUsecase) Get(ctx context.Context, req *bookPb.GetBookRequest) (*bookPb.Book, error) {
	//* Get book data
	bookData, err := b.Repo.Core.Book.Get(req)
	if err != nil {
		return nil, errors.New("error fetch data")
	}

	data := &bookPb.Book{
		ID:    bookData.BookID,
		Title: bookData.BookName,
	}

	return data, nil
}
