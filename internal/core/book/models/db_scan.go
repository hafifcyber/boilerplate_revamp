package models

type Book struct {
	BookID   int32  `json:"book_id"`
	BookName string `json:"book_name"`
}
