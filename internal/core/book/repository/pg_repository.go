package repository

import (
	"boilerplate/internal/core/book/models"
	bookPb "boilerplate/internal/grpc/pb/book"
	"boilerplate/pkg/infra/db"
)

type Repository interface {
	GetAll() (*[]models.Book, error)
	Get(req *bookPb.GetBookRequest) (*models.Book, error)
}

type BookRepo struct {
	DBList *db.DatabaseList
}

func NewBookRepo(dbList *db.DatabaseList) BookRepo {
	return BookRepo{
		DBList: dbList,
	}
}

func (r BookRepo) GetAll() (*[]models.Book, error) {
	var response []models.Book
	err := r.DBList.PostgreDB.Raw(qGetAllBooks).Scan(&response).Error
	if err != nil {
		return nil, err
	}

	return &response, err
}

func (r BookRepo) Get(req *bookPb.GetBookRequest) (*models.Book, error) {
	var response models.Book
	err := r.DBList.PostgreDB.Raw(qGetBook, req.ID).Scan(&response).Error
	if err != nil {
		return nil, err
	}

	return &response, err
}
