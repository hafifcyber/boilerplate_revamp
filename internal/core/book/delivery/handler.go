package delivery

import (
	"boilerplate/config"
	bookPb "boilerplate/internal/grpc/pb/book"
	"boilerplate/internal/wrapper/usecase"
	"context"
	"errors"
	"github.com/sirupsen/logrus"
	"google.golang.org/protobuf/types/known/emptypb"
)

type BookHandler struct {
	Usecase usecase.Usecase
	Conf    *config.Config
	Log     *logrus.Logger
	bookPb.UnimplementedBooksServer
}

func NewBookHandler(uc usecase.Usecase, conf *config.Config, logger *logrus.Logger) BookHandler {
	return BookHandler{
		Usecase:                  uc,
		Conf:                     conf,
		Log:                      logger,
		UnimplementedBooksServer: bookPb.UnimplementedBooksServer{},
	}
}

func (h BookHandler) GetAll(ctx context.Context, req *emptypb.Empty) (*bookPb.GetBooksResponse, error) {
	//* Usecase
	data, errData := h.Usecase.Core.Book.GetAll(ctx, req)
	if errData != nil {
		//* Return Error Response
		return nil, errors.New("error fetch data")
	}

	//* Success Response
	respData := &bookPb.GetBooksResponse{
		Books: data,
	}

	return respData, nil
}

func (h BookHandler) Get(ctx context.Context, req *bookPb.GetBookRequest) (*bookPb.GetBookResponse, error) {
	//* Usecase
	data, errData := h.Usecase.Core.Book.Get(ctx, req)
	if errData != nil {
		//* Return Error Response
		return nil, errors.New("error fetch data")
	}

	//* Success Response
	respData := &bookPb.GetBookResponse{
		Book: data,
	}

	return respData, nil
}
