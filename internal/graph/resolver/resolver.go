package resolver

//go:generate go run github.com/99designs/gqlgen generate

import userPb "boilerplate/internal/grpc/pb/user"
import bookPb "boilerplate/internal/grpc/pb/book"

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	GRPCUserClient userPb.UsersClient
	GRPCBookClient bookPb.BooksClient
}
