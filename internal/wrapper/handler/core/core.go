package core

import (
	"boilerplate/config"
	"boilerplate/internal/wrapper/usecase"

	book "boilerplate/internal/core/book/delivery"
	user "boilerplate/internal/core/user/delivery"
	"github.com/sirupsen/logrus"
)

type CoreHandler struct {
	User user.UserHandler
	Book book.BookHandler
}

func NewCoreHandler(uc usecase.Usecase, conf *config.Config, log *logrus.Logger) CoreHandler {
	return CoreHandler{
		User: user.NewUserHandler(uc, conf, log),
		Book: book.NewBookHandler(uc, conf, log),
	}
}
