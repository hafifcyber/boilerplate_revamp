package core

import (
	"boilerplate/config"
	"boilerplate/internal/wrapper/repository"
	"boilerplate/pkg/infra/db"

	book "boilerplate/internal/core/book/usecase"
	user "boilerplate/internal/core/user/usecase"
	"github.com/sirupsen/logrus"
)

type CoreUsecase struct {
	User user.Usecase
	Book book.Usecase
}

func NewCoreUsecase(repo repository.Repository, conf *config.Config, dbList *db.DatabaseList, log *logrus.Logger) CoreUsecase {
	return CoreUsecase{
		User: user.NewUserUsecase(repo, conf, dbList, log),
		Book: book.NewBookUsecase(repo, conf, dbList, log),
	}
}
