package core

import (
	"boilerplate/config"
	"boilerplate/pkg/infra/db"

	book "boilerplate/internal/core/book/repository"
	user "boilerplate/internal/core/user/repository"
	"github.com/sirupsen/logrus"
)

type CoreRepository struct {
	User user.Repository
	Book book.Repository
}

func NewCoreRepository(conf *config.Config, dbList *db.DatabaseList, log *logrus.Logger) CoreRepository {
	return CoreRepository{
		User: user.NewUserRepo(dbList),
		Book: book.NewBookRepo(dbList),
	}
}
