package db

import (
	"boilerplate/config"
	"boilerplate/pkg/utils"

	"github.com/sirupsen/logrus"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	gormlog "gorm.io/gorm/logger"
)

type DatabaseList struct {
	PostgreDB *gorm.DB
	StooneDB  *gorm.DB
}

func NewGORMConnection(conf *config.DatabaseType, log *logrus.Logger, dbType string) *gorm.DB {

	var db *gorm.DB
	var err error

	if dbType == "StooneDB" {
		//* Get DBName from DB source
		dbName := utils.GetDBNameFromDriverSource(conf.StooneDB.DriverSource)
		//* Open Connection depend on driver
		if conf.StooneDB.DriverName == "stoone" || conf.StooneDB.DriverName == "pgx" {
			db, err = gorm.Open(postgres.Open(conf.StooneDB.DriverSource), &gorm.Config{
				Logger: gormlog.Default.LogMode(gormlog.LogLevel(gormlog.Error)),
			})
		}

		if err != nil {
			log.Fatal("Failed to connect database " + dbName + ", err: " + err.Error())
		}
	}

	if dbType == "PostgreDB" {
		//* Get DBName from DB source
		dbName := utils.GetDBNameFromDriverSource(conf.PostgreDB.DriverSource)
		//* Open Connection depend on driver
		if conf.PostgreDB.DriverName == "postgres" || conf.PostgreDB.DriverName == "pgx" {
			db, err = gorm.Open(postgres.Open(conf.PostgreDB.DriverSource), &gorm.Config{
				Logger: gormlog.Default.LogMode(gormlog.LogLevel(gormlog.Error)),
			})
		}

		if err != nil {
			log.Fatal("Failed to connect database " + dbName + ", err: " + err.Error())
		}
	}

	log.Info("Connection Opened to Database " + conf.PostgreDB.DriverName)
	return db
}
