# ==============================================================================
# Golang commands
run:
	echo "Running env local"
	go run cmd/api/main.go

# ==============================================================================
rungrpc:
	go run ./cmd/grpc/main.go

rungql:
	go run ./cmd/graphql/main.go

generate-protoc-user:
	protoc -I./internal/grpc/protobuf \
	--go_out=./internal/grpc/pb --go_opt=paths=source_relative \
	--go-grpc_out=./internal/grpc/pb --go-grpc_opt=paths=source_relative \
	internal/grpc/protobuf/user/*.proto

generate-protoc-book:
	protoc -I./internal/grpc/protobuf \
	--go_out=./internal/grpc/pb --go_opt=paths=source_relative \
	--go-grpc_out=./internal/grpc/pb --go-grpc_opt=paths=source_relative \
	internal/grpc/protobuf/book/*.proto

go-gen:
	go generate ./...